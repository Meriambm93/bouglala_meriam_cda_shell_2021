#!/bin/bash
echo "Choix 1: Créer un utilisateur "
echo "Choix 2: Supprimer un utilisateur "

read -p "Veuillez choisir entre le choix 1 et 2 : " choice 
read -p "Veuillez entrer un nom d'utilisateur : " username

if id "$username" &>/dev/null;
then
  is_username_exist=true
else
  is_username_exist=false
fi

case $choice in
  "1")
    if [ "$is_username_exist" = false ]
    then
      sudo useradd $username
      echo "L'utilisateur $username a été crée"
    else
      echo "L'utilisateur $username existe déjà"
    fi
    ;;
 
  "2")
    if [ "$is_username_exist" = true ]
    then
      sudo userdel $username 
      echo "L'utilisateur $username a été supprimé"
    else
      echo "L'utilisateur $username n'existe pas"
    fi
    ;;
esac
