#!/bin/bash
echo "Choix 1: Créer un groupe "
echo "Choix 2: Supprimer un groupe "

read -p "Veuillez choisir entre le choix 1 et 2 : " choice 
read -p "Veuillez entrer un nom de groupe : " groupname

if grep -q $groupname /etc/group
then
  is_groupname_exist=true
else
  is_groupname_exist=false
fi

case $choice in
  "1")
    if [ "$is_groupname_exist" = false ]
    then
      sudo useradd $groupname
      echo "Le groupe $groupname a été crée"
    else
      echo "Le groupe $groupname existe déjà"
    fi
    ;;
 
  "2")
    if [ "$is_groupname_exist" = true ]
    then
      sudo userdel $groupname 
      echo "Le groupe $groupname a été supprimé"
    else
      echo "Le groupe $groupname n'existe pas"
    fi
    ;;
esac
