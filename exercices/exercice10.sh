#!/bin/bash
function continuer
{
    echo "Appuyez sur entrée pour continuer"
    read
}
 
function existenceUser
{
    echo "Saisir le nom de l'utilisateur"
    read user
}
 
function uidUser
{
    if grep "^$user:" /etc/passwd > /dev/null
    then
        echo "L'utilisateur existe"
    else
        echo "L'utilisateur n'existe pas"
    fi
    continuer
}
 
rep=1
while [ $rep -eq 1 ]
do
    clear
    printf "menu :\n\n"
    echo "Veuillez saisir le chiffre 1 pour vérifier l'existence d'un utilisateur"
    echo "Veuillez saisir le chiffre 2 pour verifier l'uid d'un utilisateur"
    echo -e "saisir la lettre q pour  Quitter\n"
    read reponse
    case $reponse in
        1)
            existenceUser
            uidUser ;;
 
        2) 
            existenceUser
            id $user
            continuer ;;
 
        q)
            echo "Merci"
            continuer
            rep=0 ;;
        *)
            echo "erreur de saisie veuillez recommencer"
                        continuer ;;
    esac
done
