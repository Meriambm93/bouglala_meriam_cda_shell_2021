#!/bin/bash

echo "Décalage d'un pas avec la commande "shift""
shift
echo "Nombre de paramètres : $#"
echo "Le 1er paramètre est : $1"
echo "Le 2ème paramètre est : $2"
echo "Le 3ème paramètre est : $3"
echo "Le 4ème paramètre est : ${4}"

